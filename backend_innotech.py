#!/usr/bin/env python
# -*- coding: utf-8 -*-
import json
from flask import Flask, request, send_file,Response
from werkzeug.utils import secure_filename
from datetime import datetime
import pyodbc

app = Flask(__name__)

dict_data = {}
dict_country = {"BD": "6", "BE": "7", "FR": "27", "DK": "22", "BM": "9", "BN": "13", "DE": "28", "BH": "5", "BJ": "8", "LK": "73", "NO": "59", "HK": "33", "JO": "42", "HN": "32", "BR": "10", "BS": "4", "FI": "26", "GR": "29", "RU": "68", "LB": "46", "PT": "67", "LA": "45", "TW": "76", "TR": "79", "NZ": "57", "LI": "49", "TO": "78", "LT": "50", "LU": "51", "LR": "47", "ES": "72", "TH": "77", "PE": "64", "NP": "55", "PK": "62", "PH": "65", "AE": "81", "IS": "34", "PL": "66", "LY": "48", "CH": "75", "CO": "20", "CN": "19", "VG": "11", "JP": "41", "CA": "16", "IR": "37", "IT": "40", "EC": "24", "CD": "23", "CY": "21", "AR": "00001", "AU": "00002", "AT": "3", "IN": "35", "VN": "83", "NL": "56", "ZW": "85", "IE": "38", "ID": "36", "HT": "31", "GH": "30", "PA": "63", "MM": "14", "SG": "71", "IL": "39", "KH": "15", "US": "80", "MU": "53", "KR": "44", "MP": "58", "WS": "69", "KY": "17", "MY": "52", "MX": "54", "SE": "74", "GB": "82"}
dict_native = {"BD": "00094", "BE": "00005", "FR": "00021", "MN": "00105", "DK": "00017", "BM": "00006", "BN": "00010", "BO": "00076", "JP": "00033", "HT": "00024", "HU": "00123", "JO": "00034", "DZ": "00082", "BR": "00007", "FI": "00122", "BY": "00107", "VE": "00120", "ES": "00075", "RU": "00078", "NL": "00018", "PT": "00047", "LA": "00037", "NA": "00119", "LK": "00052", "TN": "00112", "TO": "00056", "PA": "00046", "LR": "00039", "PG": "00114", "TH": "00000", "PE": "00048", "NP": "00100", "PK": "00045", "EG": "00111", "LY": "00110", "CI": "00108", "DE": "00022", "CN": "00014", "IQ": "00096", "CA": "00011", "IR": "00080", "AL": "00091", "AO": "00092", "CD": "00116", "CY": "00016", "AR": "00002", "AU": "00071", "GB": "00072", "IN": "00029", "VN": "00057", "ET": "00083", "ZW": "00104", "IE": "00125", "UG": "00115", "CU": "00095", "SY": "00085", "KE": "00084", "MM": "00009", "UA": "00113", "IL": "00031", "KH": "00015", "CH": "00054", "US": "00001", "MU": "00074", "KR": "00126", "KP": "00081", "KW": "00097", "TZ": "00086", "TW": "00055", "HN": "00025", "IT": "00032", "SD": "00102", "SG": "00050", "MX": "00043", "SE": "00053", "AT": "00003"}
dict_occ = {"160": "A00051", "130": "A00003", "70": "110000", "40": "A00001", "170": "A00030", "140": "A00051", "110": "A00051", "80": "A00051", "50": "A00051", "20": "A00051", "150": "A00051", "120": "A00051", "25": "A00051", "90": "A00051", "60": "A00051", "30": "A00005"}


def json_response(messages=None, status=None, headers=None):
    if headers == None:
        headers = dict()
    headers.update({"Content-Type": "application/json"})
    contents = json.dumps(messages)
    if contents[0] == "'" and contents[len(contents)] == "'":
        contents.replace("'", '"')
    if status == None:
        status = 200
    resp = Response(response=contents, headers=headers, status=int(status))

    return resp


@app.after_request
def after_request(response):
    response.headers.add('Access-Control-Allow-Origin', '*')
    response.headers.add('Access-Control-Expose-Headers', 'X-Token')
    response.headers.add('Access-Control-Allow-Headers', 'Content-Type,Authorization')
    response.headers.add('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE,OPTIONS')
    return response

def repr_dict(d):
    return '{%s}' % ',\n'.join("'%s': '%s'" % pair for pair in d.iteritems())

def readfile(part):
    with open(part) as file:
        data = file.read()
        return data

def setpattern_account(text):
    str = ''
    header = text[0].split('|')
    for j in range(1, len(text)):
        data = text[j].split('|')
        # num = 1
        dict_data['REF_TYPE'] = data[16].decode('utf-8')
        dict_data['REF_ID'] = data[19].decode('utf-8')
        dict_data['TAX_ID'] = data[19].decode('utf-8')
        dict_data['HOLDER_REF_ID'] = data[1].decode('utf-8')

        dict_data['TITLE_ID'] = ''
        if data[11].__contains__("MRS"):
            dict_data['TITLE_ID'] = "MRS".decode('utf-8')
        elif data[11].__contains__("MR"):
            dict_data['TITLE_ID'] = "MR".decode('utf-8')
        elif data[11].__contains__("MIS"):
            dict_data['TITLE_ID'] = "MIS".decode('utf-8')
        else:
            dict_data['TITLE_ID'] = "OTH".decode('utf-8')

        dict_data['TITLE_NAME'] = data[10].decode('utf-8')
        dict_data['FIRST_NAME'] = data[12].decode('utf-8')
        dict_data['LAST_NAME'] = data[13].decode('utf-8')
        dict_data['TITLE_NAME_E'] = data[11].decode('utf-8')
        dict_data['FIRST_NAME_E'] = data[14].decode('utf-8')
        dict_data['LAST__NAME_E'] = data[15].decode('utf-8')
        dict_data['HLD_TYPE'] = ''
        if data[5] == '1':
            dict_data['HLD_TYPE'] = 'P'.decode('utf-8')
        elif data[5] == '2':
            dict_data['HLD_TYPE'] = 'C'.decode('utf-8')
        dict_data['ADDR1'] = data[60].decode('utf-8') + ' ' + data[61].decode('utf-8') + ' ' + data[62].decode(
            'utf-8') + ' ' + data[63].decode('utf-8') + ' ' + data[64].decode('utf-8') + ' ' + data[65].decode('utf-8')
        dict_data['ADDR2'] = data[66].decode('utf-8') + ' ' + data[67].decode('utf-8') + ' ' + data[68].decode('utf-8')
        dict_data['ZIP_CODE'] = data[69].decode('utf-8')
        dict_data['ADDR_REG1'] = data[35].decode('utf-8') + ' ' + data[36].decode('utf-8') + ' ' + data[37].decode(
            'utf-8') + ' ' + data[38].decode('utf-8') + ' ' + data[39].decode('utf-8') + ' ' + data[40].decode('utf-8')
        dict_data['ADDR_REG2'] = data[41].decode('utf-8') + ' ' + data[42].decode('utf-8') + ' ' + data[43].decode('utf-8')
        dict_data['ZIP_CODE_REG'] = ''
        if data[45] != 'TH':
            dict_data['ZIP_CODE_REG'] = '99999'.decode('utf-8')

        dict_data['COUNTRY_ID_REG'] = ''
        if dict_country.has_key(data[45]):
            dict_data['COUNTRY_ID_REG'] = dict_country[data[45]].decode('utf-8')
        else:
            dict_data['COUNTRY_ID_REG'] = "60".decode('utf-8')

        dict_data['TEL_HOME'] = data[46].decode('utf-8')
        dict_data['TEL_OFFICE'] = data[59].decode('utf-8')
        dict_data['TEL_MOBILE'] = data[27].decode('utf-8')
        dict_data['FAX'] = ''.decode('utf-8')
        dict_data['E_MAIL'] = data[26].decode('utf-8')
        dict_data['BIRTH_DATE'] = data[23].decode('utf-8')
        dict_data['SEX_ID'] = ''.decode('utf-8')
        if data[5] == '2':
            dict_data['SEX_ID'] = 'O'.decode('utf-8')
        elif data[9] == 'M':
            dict_data['SEX_ID'] = 'M'.decode('utf-8')
        elif data[9] == 'F':
            dict_data['SEX_ID'] = 'F'.decode('utf-8')

        dict_data['NATIVE_ID'] = ''
        if dict_native.has_key(data[24]):
            dict_data['NATIVE_ID'] = dict_native[data[24]].decode('utf-8')
        else:
            dict_data['NATIVE_ID'] = "00099".decode('utf-8')

        dict_data['OCCUPATION_ID'] = ''
        if dict_occ.has_key(data[72]):
            dict_data['OCCUPATION_ID'] = dict_occ[data[72]].decode('utf-8')
        else:
            dict_data['OCCUPATION_ID'] = "A00030".decode('utf-8')

        dict_data['OPEN_DATE'] = header[0].decode('utf-8')
        dict_data['AGENT_HEAD'] = data[4].decode('utf-8')
        dict_data['AGENT_BRANCH'] = data[102].decode('utf-8')
        dict_data['ADVISOR_CODE'] = ''.decode('utf-8')
        dict_data['IC_CODE'] = data[101].decode('utf-8')
        dict_data['PUR_ATS_FLAG'] = 'N'.decode('utf-8')
        dict_data['RED_BANK_FLAG'] = 'Y'.decode('utf-8')
        dict_data['CHQ_METHOD'] = 'P'.decode('utf-8')
        dict_data['REINV_AUTORED'] = 'N'.decode('utf-8')
        dict_data['REINV_DIVIDEND'] = 'N'.decode('utf-8')
        dict_data['WTH_TAX_FLAG'] = data[83].decode('utf-8')
        dict_data['STATUS'] = ''
        if data[0] == 'N':
            dict_data['STATUS'] = 'I'.decode('utf-8')
        elif data[0] == 'U':
            dict_data['STATUS'] = 'U'.decode('utf-8')
        dict_data['DOC_METHOD'] = ''.decode('utf-8')
        if data[96] == '1':
            dict_data['DOC_METHOD'] = 'Email'.decode('utf-8')
        elif data[96] == '2':
            dict_data['DOC_METHOD'] = 'Mail'.decode('utf-8')
        dict_data['RISK_LEVEL'] = data[85].decode('utf-8')
        dict_data['HEAD_OFFICE'] = 'Y'.decode('utf-8')
        dict_data['BRANCH_NO'] = data[102].decode('utf-8')
        dict_data['OMNIBUS_ACCOUNT'] = ''.decode('utf-8')
        if data[2] == "OMN":
            dict_data['OMNIBUS_ACCOUNT'] = 'Y'.decode('utf-8')
        else:
            dict_data['OMNIBUS_ACCOUNT'] = 'N'.decode('utf-8')


        # print (repr_dict(dict_data))

        str += dict_data['REF_TYPE'][:1]
        if len(dict_data['REF_TYPE']) < 1:
            for i in xrange(1 - len(dict_data['REF_TYPE'])):
                str += ' '

        str += dict_data['REF_ID'][:13]
        if len(dict_data['REF_ID']) < 13:
            for i in xrange(13 - len(dict_data['REF_ID'])):
                str += ' '

        str += dict_data['TAX_ID'][:13]
        if len(dict_data['TAX_ID']) < 13:
            for i in xrange(13 - len(dict_data['TAX_ID'])):
                str += ' '

        str += dict_data['HOLDER_REF_ID'][:15]
        if len(dict_data['HOLDER_REF_ID']) < 15:
            for i in xrange(15 - len(dict_data['HOLDER_REF_ID'])):
                str += ' '

        str += dict_data['TITLE_ID'][:3]
        if len(dict_data['TITLE_ID']) < 3:
            for i in xrange(3 - len(dict_data['TITLE_ID'])):
                str += ' '

        str += dict_data['TITLE_NAME'][:30]
        if len(dict_data['TITLE_NAME']) < 30:
            for i in xrange(30 - len(dict_data['TITLE_NAME'])):
                str += ' '

        str += dict_data['FIRST_NAME'][:40]
        if len(dict_data['FIRST_NAME']) < 40:
            for i in xrange(40 - len(dict_data['FIRST_NAME'])):
                str += ' '

        str += dict_data['LAST_NAME'][:110]
        if len(dict_data['LAST_NAME']) < 110:
            for i in xrange(110 - len(dict_data['LAST_NAME'])):
                str += ' '

        str += dict_data['TITLE_NAME_E'][:30]
        if len(dict_data['TITLE_NAME_E']) < 30:
            for i in xrange(30 - len(dict_data['TITLE_NAME_E'])):
                str += ' '

        str += dict_data['FIRST_NAME_E'][:40]
        if len(dict_data['FIRST_NAME_E']) < 40:
            for i in xrange(40 - len(dict_data['FIRST_NAME_E'])):
                str += ' '

        str += dict_data['LAST__NAME_E'][:110]
        if len(dict_data['LAST__NAME_E']) < 110:
            for i in xrange(110 - len(dict_data['LAST__NAME_E'])):
                str += ' '

        str += dict_data['HLD_TYPE'][:1]
        if len(dict_data['HLD_TYPE']) < 1:
            for i in xrange(1 - len(dict_data['HLD_TYPE'])):
                str += ' '

        str += dict_data['ADDR1'][:70]
        if len(dict_data['ADDR1']) < 70:
            for i in xrange(70 - len(dict_data['ADDR1'])):
                str += ' '

        str += dict_data['ADDR2'][:70]
        if len(dict_data['ADDR2']) < 70:
            for i in xrange(70 - len(dict_data['ADDR2'])):
                str += ' '

        str += dict_data['ZIP_CODE'][:5]
        if len(dict_data['ZIP_CODE']) < 5:
            for i in xrange(5 - len(dict_data['ZIP_CODE'])):
                str += ' '

        str += dict_data['ADDR_REG1'][:70]
        if len(dict_data['ADDR_REG1']) < 70:
            for i in xrange(70 - len(dict_data['ADDR_REG1'])):
                str += ' '

        str += dict_data['ADDR_REG2'][:70]
        if len(dict_data['ADDR_REG2']) < 70:
            for i in xrange(70 - len(dict_data['ADDR_REG2'])):
                str += ' '

        str += dict_data['ZIP_CODE_REG'][:5]
        if len(dict_data['ZIP_CODE_REG']) < 5:
            for i in xrange(5 - len(dict_data['ZIP_CODE_REG'])):
                str += ' '

        str += dict_data['COUNTRY_ID_REG'][:5]
        if len(dict_data['COUNTRY_ID_REG']) < 5:
            for i in xrange(5 - len(dict_data['COUNTRY_ID_REG'])):
                str += ' '

        str += dict_data['TEL_HOME'][:30]
        if len(dict_data['TEL_HOME']) < 30:
            for i in xrange(30 - len(dict_data['TEL_HOME'])):
                str += ' '

        str += dict_data['TEL_OFFICE'][:30]
        if len(dict_data['TEL_OFFICE']) < 30:
            for i in xrange(30 - len(dict_data['TEL_OFFICE'])):
                str += ' '

        str += dict_data['TEL_MOBILE'][:30]
        if len(dict_data['TEL_MOBILE']) < 30:
            for i in xrange(30 - len(dict_data['TEL_MOBILE'])):
                str += ' '

        str += dict_data['FAX'][:30]
        if len(dict_data['FAX']) < 30:
            for i in xrange(30 - len(dict_data['FAX'])):
                str += ' '

        str += dict_data['E_MAIL'][:50]
        if len(dict_data['E_MAIL']) < 50:
            for i in xrange(50 - len(dict_data['E_MAIL'])):
                str += ' '

        str += dict_data['BIRTH_DATE'][:10]
        if len(dict_data['BIRTH_DATE']) < 10:
            for i in xrange(10 - len(dict_data['BIRTH_DATE'])):
                str += ' '

        str += dict_data['SEX_ID'][:1]
        if len(dict_data['SEX_ID']) < 1:
            for i in xrange(1 - len(dict_data['SEX_ID'])):
                str += ' '

        str += dict_data['NATIVE_ID'][:5]
        if len(dict_data['NATIVE_ID']) < 5:
            for i in xrange(5 - len(dict_data['NATIVE_ID'])):
                str += ' '

        str += dict_data['OCCUPATION_ID'][:6]
        if len(dict_data['OCCUPATION_ID']) < 6:
            for i in xrange(6 - len(dict_data['OCCUPATION_ID'])):
                str += ' '

        str += dict_data['OPEN_DATE'][:10]
        if len(dict_data['OPEN_DATE']) < 10:
            for i in xrange(10 - len(dict_data['OPEN_DATE'])):
                str += ' '

        str += dict_data['AGENT_HEAD'][:5]
        if len(dict_data['AGENT_HEAD']) < 5:
            for i in xrange(5 - len(dict_data['AGENT_HEAD'])):
                str += ' '

        str += dict_data['AGENT_BRANCH'][:5]
        if len(dict_data['AGENT_BRANCH']) < 5:
            for i in xrange(5 - len(dict_data['AGENT_BRANCH'])):
                str += ' '

        str += dict_data['ADVISOR_CODE'][:30]
        if len(dict_data['ADVISOR_CODE']) < 30:
            for i in xrange(30 - len(dict_data['ADVISOR_CODE'])):
                str += ' '

        str += dict_data['IC_CODE'][:10]
        if len(dict_data['IC_CODE']) < 10:
            for i in xrange(10 - len(dict_data['IC_CODE'])):
                str += ' '

        str += dict_data['PUR_ATS_FLAG'][:1]
        if len(dict_data['PUR_ATS_FLAG']) < 1:
            for i in xrange(1 - len(dict_data['PUR_ATS_FLAG'])):
                str += ' '

        str += dict_data['RED_BANK_FLAG'][:1]
        if len(dict_data['RED_BANK_FLAG']) < 1:
            for i in xrange(1 - len(dict_data['RED_BANK_FLAG'])):
                str += ' '

        str += dict_data['CHQ_METHOD'][:1]
        if len(dict_data['CHQ_METHOD']) < 1:
            for i in xrange(1 - len(dict_data['CHQ_METHOD'])):
                str += ' '

        str += dict_data['REINV_AUTORED'][:1]
        if len(dict_data['REINV_AUTORED']) < 1:
            for i in xrange(1 - len(dict_data['REINV_AUTORED'])):
                str += ' '

        str += dict_data['REINV_DIVIDEND'][:1]
        if len(dict_data['REINV_DIVIDEND']) < 1:
            for i in xrange(1 - len(dict_data['REINV_DIVIDEND'])):
                str += ' '

        str += dict_data['WTH_TAX_FLAG'][:1]
        if len(dict_data['WTH_TAX_FLAG']) < 1:
            for i in xrange(1 - len(dict_data['WTH_TAX_FLAG'])):
                str += ' '

        str += dict_data['STATUS'][:1]
        if len(dict_data['STATUS']) < 1:
            for i in xrange(1 - len(dict_data['STATUS'])):
                str += ' '

        str += dict_data['DOC_METHOD'][:10]
        if len(dict_data['DOC_METHOD']) < 10:
            for i in xrange(10 - len(dict_data['DOC_METHOD'])):
                str += ' '

        str += dict_data['RISK_LEVEL'][:1]
        if len(dict_data['RISK_LEVEL']) < 1:
            for i in xrange(1 - len(dict_data['RISK_LEVEL'])):
                str += ' '

        str += dict_data['HEAD_OFFICE'][:1]
        if len(dict_data['HEAD_OFFICE']) < 1:
            for i in xrange(1 - len(dict_data['HEAD_OFFICE'])):
                str += ' '

        str += dict_data['BRANCH_NO'][:5]
        if len(dict_data['BRANCH_NO']) < 5:
            for i in xrange(5 - len(dict_data['BRANCH_NO'])):
                str += ' '

        str += dict_data['OMNIBUS_ACCOUNT'][:1]
        if len(dict_data['OMNIBUS_ACCOUNT']) < 1:
            for i in xrange(1 - len(dict_data['OMNIBUS_ACCOUNT'])):
                str += ' '

        if j != len(text)-1:
            str += '\n'


    return str

def setpattern_navbay():
    server = '192.168.100.5'
    database = 'Zmico_Asset'
    username = 'Inet'
    password = 'Inet$999'

    time_data = datetime.now().strftime("%Y%m%d")
    cnxn = pyodbc.connect('DRIVER={SQL Server};SERVER='+server+';DATABASE='+database+';UID='+username+';PWD='+ password)
    cursor = cnxn.cursor()

    script = """SELECT CONVERT(char(10), T_DAILY_NAV.DTENAVDATE,126) AS TRANS_DATE, ' ' + T_DAILY_NAV.STRFUNDREF AS FUND_ID , RIGHT('000000000000000'+ CONVERT(VARCHAR,CONVERT(NUMERIC(12,4),T_DAILY_NAV.DECNAV_UNIT)),15) AS NAV_VALUE, RIGHT('000000000000000'+ CONVERT(VARCHAR,CONVERT(NUMERIC(12,4),T_DAILY_NAV.DECNAV)),15) AS ASSET_VALUE, 'P' AS TYPE, 'N' AS SUB_TYPE, ' ' AS SAME_AMC, ' ' AS CHANNEL_TYPE, 'Y' AS FEE_FLAG, ' 1' AS SEQ, RIGHT('000000000000000'+ CONVERT(VARCHAR,CONVERT(NUMERIC(12,4),T_DAILY_NAV.DECPURCHASE)),15) AS NAV, RIGHT('000000000000000'+ CONVERT(VARCHAR,CONVERT(NUMERIC(12,4), T_DAILY_NAV.DECPURCHASE - T_DAILY_NAV.DECNAV_UNIT )),15) AS SPREAD_VALUE , 'I' AS STATUS FROM T_DAILY_NAV LEFT OUTER JOIN M_FUND ON T_DAILY_NAV.STRFUNDREF = M_FUND.StrFundREF LEFT OUTER JOIN M_FUND_POLICY ON T_DAILY_NAV.STRFUNDREF = M_FUND_POLICY.StrFundREF WHERE (T_DAILY_NAV.DTENAVDATE = '20190902' ) and T_DAILY_NAV.STRVERIFYFLAG='A' and M_FUND.StrVerifyFlag = 'A' UNION ALL SELECT CONVERT(char(10), T_DAILY_NAV.DTENAVDATE,126) AS TRANS_DATE, ' ' + T_DAILY_NAV.STRFUNDREF AS FUND_ID , RIGHT('000000000000000'+ CONVERT(VARCHAR,CONVERT(NUMERIC(12,4),T_DAILY_NAV.DECNAV_UNIT)),15) AS NAV_VALUE, RIGHT('000000000000000'+ CONVERT(VARCHAR,CONVERT(NUMERIC(12,4),T_DAILY_NAV.DECNAV)),15) AS ASSET_VALUE, 'R' AS TYPE, 'N' AS SUB_TYPE, ' ' AS SAME_AMC, ' ' AS CHANNEL_TYPE, 'Y' AS FEE_FLAG, ' 1' AS SEQ, RIGHT('000000000000000'+ CONVERT(VARCHAR,CONVERT(NUMERIC(12,4),T_DAILY_NAV.DECREDEEM)),15) AS NAV, RIGHT('000000000000000'+ CONVERT(VARCHAR,CONVERT(NUMERIC(12,4), T_DAILY_NAV.DECNAV_UNIT - T_DAILY_NAV.DECREDEEM )),15) AS SPREAD_VALUE , 'I' AS STATUS FROM T_DAILY_NAV LEFT OUTER JOIN M_FUND ON T_DAILY_NAV.STRFUNDREF = M_FUND.StrFundREF LEFT OUTER JOIN M_FUND_POLICY ON T_DAILY_NAV.STRFUNDREF = M_FUND_POLICY.StrFundREF WHERE (T_DAILY_NAV.DTENAVDATE = '"""+time_data+"""' ) and T_DAILY_NAV.STRVERIFYFLAG='A' and M_FUND.StrVerifyFlag = 'A' --SELECT * FROM T_DAILY_NAV WHERE STRFUNDREF = '001' order by dtenavdate"""
    #cursor.execute('SELECT * FROM information_schema.COLUMNS;')

    cursor.execute(script)

     #with open(file_name, 'w') as file:
      #      file.write(res.encode('utf-8'))

    data = list(cursor.fetchall())


    str = ''
    num = 1
    for row in data:
        count = 1
        for text in row:
            text = text.strip()
            if count == 1:
                str += text[:10]
                if len(text) < 10:
                    for i in xrange(10 - len(text)):
                        str += ' '
            elif count == 2:
                str += text[:5]
                if len(text) < 5:
                    for i in xrange(5 - len(text)):
                        str += ' '
            elif count == 3:
                str += text[:15]
                if len(text) < 15:
                    for i in xrange(15 - len(text)):
                        str += ' '
            elif count == 4:
                str += text[:15]
                if len(text) < 15:
                    for i in xrange(15 - len(text)):
                        str += ' '
            elif count == 5:
                str += text[:1]
                if len(text) < 1:
                    for i in xrange(1 - len(text)):
                        str += ' '
            elif count == 6:
                str += text[:1]
                if len(text) < 1:
                    for i in xrange(1 - len(text)):
                        str += ' '
            elif count == 7:
                str += text[:1]
                if len(text) < 1:
                    for i in xrange(1 - len(text)):
                        str += ' '
            elif count == 8:
                str += text[:1]
                if len(text) < 1:
                    for i in xrange(1 - len(text)):
                        str += ' '
            elif count == 9:
                str += text[:1]
                if len(text) < 1:
                    for i in xrange(1 - len(text)):
                        str += ' '
            elif count == 10:
                str += text[:5]
                if len(text) < 5:
                    for i in xrange(5 - len(text)):
                        str += ' '
            elif count == 11:
                str += text[:15]
                if len(text) < 15:
                    for i in xrange(15 - len(text)):
                        str += ' '
            elif count == 12:
                str += text[:15]
                if len(text) < 15:
                    for i in xrange(15 - len(text)):
                        str += ' '
            elif count == 13:
                str += text[:1]
                if len(text) < 1:
                    for i in xrange(1 - len(text)):
                        str += ' '
            count += 1
        if num != len(data):
            str += '\n'
        num += 1

    return str

def setpattern_approved(text):
    output = []
    for i in range(len(text)):
        if i == 0:
            header = text[0]
            # print 'Header: ', header
        else:
            if text[i] != '':
                data = text[i].split('|')
                tr_type = data[6][0]
                # print tr_type
                tr_subtype = ' ' * 1
                # print tr_subtype
                fund_id = data[7].split('-')[1][0:5]
                if fund_id == 'LTF':
                    fund_id = '001  '
                    # print fund_id
                elif fund_id == 'EQRMF':
                    fund_id = '016  '
                    # print fund_id
                agent_head = data[3][0:5]
                if len(agent_head.strip()) < 5:
                    agent_head = agent_head + (' ' * (5 - len(agent_head.strip())))
                # print agent_head
                agent_branch = data[22][0:5]
                if len(agent_branch.strip()) < 5:
                    agent_branch = agent_branch + (' ' * (5 - len(agent_branch.strip())))
                    # print agent_branch
                holder_ref_id = data[4][0:15]
                if len(holder_ref_id.strip()) < 15:
                    holder_ref_id = holder_ref_id + (' ' * (15 - len(holder_ref_id.strip())))
                    # print holder_ref_id
                fundacc_ref_id = data[46][0:15]
                if len(fundacc_ref_id.strip()) < 15:
                    fundacc_ref_id = fundacc_ref_id + (' ' * (15 - len(fundacc_ref_id.strip())))
                    # print fundacc_ref_id
                effective_date = data[13]
                effective_date = effective_date[0:4] + '-' + effective_date[4:6] + '-' + effective_date[6:8]
                # print effective_date
                trans_flag = data[24][0]
                # print trans_flag
                trans_ref_seq = data[0][0:20]
                if len(trans_ref_seq.strip()) < 20:
                    trans_ref_seq = trans_ref_seq + (' ' * (20 - len(trans_ref_seq.strip())))
                channel = data[23][0:2]
                if len(channel.strip()) < 2:
                    channel = channel + (' ' * (2 - len(channel.strip())))
                # print channel
                order_type = data[10][0]
                # print order_type
                order_unit = data[12][0:19]
                if len(order_unit.strip()) < 19:
                    order_unit = order_unit + (' ' * (19 - len(order_unit.strip())))
                # print order_unit
                order_amt = data[11][0:19]
                if len(order_amt.strip()) < 19:
                    order_amt = order_amt + (' ' * (19 - len(order_amt.strip())))
                # print order_amt
                amc_flag = ' ' * 1
                # print amc_flag
                benefit_order = ' ' * 19
                # print benefit_order
                first_pur_date = ' ' * 10
                # print first_pur_date
                advisor_code = ' ' * 30
                # print advisor_code
                advice_flag = ' ' * 1
                # print advice_flag
                ic_code = data[21][0:10]
                if len(ic_code.strip()) < 10:
                    ic_code = ic_code + (' ' * (10 - len(ic_code.strip())))
                # print ic_code
                reason_flag = data[26]
                if len(reason_flag.strip()) < 1:
                    reason_flag = reason_flag + (' ' * (1 - len(reason_flag.strip())))
                # print reason_flag
                reason = ' ' * 30
                # print reason
                pmnt_seq = data[30][0:2]
                if len(pmnt_seq.strip()) < 2:
                    pmnt_seq = pmnt_seq + (' ' * (2 - len(pmnt_seq.strip())))
                pmnt_type = data[16][0]
                if len(pmnt_type.strip()) < 1:
                    pmnt_type = pmnt_type + (' ' * (1 - len(pmnt_type.strip())))
                # print pmnt_type
                pmnt_value = data[35][0:19]
                if len(pmnt_value.strip()) < 19:
                    pmnt_value = pmnt_value + (' ' * (19 - len(pmnt_value.strip())))
                # print pmnt_value
                bank_id = data[45][0:3]
                if len(bank_id.strip()) < 3:
                    bank_id = bank_id + (' ' * (3 - len(bank_id.strip())))
                # print bank_id
                branch_id = data[48][0:4]
                if len(branch_id.strip()) < 4:
                    branch_id = branch_id + (' ' * (4 - len(branch_id.strip())))
                # print branch_id
                acct_type = ' ' * 1
                # print acct_type
                if len(data) > 71:
                    acct_id = data[71]
                    if len(acct_id.strip()) < 15:
                        acct_id = acct_id + (' ' * (15 - len(acct_id.strip())))
                else:
                    acct_id = ' ' * 15
                # print acct_id
                acct_name = ' ' * 100
                # print acct_name
                cheque_id = data[19][0:10]
                if len(cheque_id.strip()) < 10:
                    cheque_id = cheque_id + (' ' * (10 - len(cheque_id.strip())))
                # print cheque_id
                chq_method = ' ' * 1
                # print chq_method
                i_fund_deal = data[7].split('-')[1][0:5]
                if i_fund_deal == 'LTF':
                    i_fund_deal = '001  '
                    # print i_fund_deal
                elif i_fund_deal == 'EQRMF':
                    i_fund_deal = '016  '
                    # print i_fund_deal
                fund_deal_name = data[14][0:100]
                if len(fund_deal_name.strip()) < 100:
                    fund_deal_name = fund_deal_name + (' ' * (100 - len(fund_deal_name.strip())))
                # print fund_deal_name
                amc_deal_name = ' ' * 100
                # print amc_deal_name
                holder_ref_deal_id = ' ' * 15
                # print holder_ref_deal_id
                fundacc_ref_deal_id = ' ' * 15
                # print fundacc_ref_deal_id
                bank_acct_pur_id = ' ' * 15
                # print bank_acct_pur_id
                bank_pur_id = ' ' * 3
                # print bank_pur_id
                branch_pur_id = ' ' * 4
                # print branch_pur_id
                liquidate_cond = data[25]
                if len(liquidate_cond.strip()) < 1:
                    liquidate_cond = liquidate_cond + (' ' * (1 - len(liquidate_cond.strip())))
                # print liquidate_cond
                amc_liquid = ' ' * 1
                # print amc_liquid
                fund_deal_liquid = ' ' * 100
                # print fund_deal_liquid
                amc_deal_liquid = ' ' * 100
                # print amc_deal_liquid
                user_id = data[4][0:10]
                if len(user_id.strip()) < 10:
                    user_id = user_id + (' ' * (10 - len(user_id.strip())))
                # print user_id
                trans_date = data[33][0:10]
                if len(trans_date.strip()) < 10:
                    trans_date = trans_date + (' ' * (10 - len(trans_date.strip())))
                # print trans_date
                trans_time = data[1][8:10] + '.' + data[1][10:12] + '.' + data[1][12:14]
                # print trans_time
                value = tr_type + tr_subtype + fund_id + agent_head + agent_branch + holder_ref_id + fundacc_ref_id \
                        + effective_date + trans_flag + trans_ref_seq + channel + order_type + order_unit + order_amt \
                        + amc_flag + benefit_order + first_pur_date + advisor_code + advice_flag + ic_code \
                        + reason_flag + reason + pmnt_seq + pmnt_type + pmnt_value + bank_id + branch_id + acct_type \
                        + acct_id + acct_name + cheque_id + chq_method + i_fund_deal + fund_deal_name + amc_deal_name \
                        + holder_ref_deal_id + fundacc_ref_deal_id + bank_acct_pur_id + bank_pur_id + branch_pur_id \
                        + liquidate_cond + amc_liquid + fund_deal_liquid + amc_deal_liquid + user_id + trans_date + trans_time
                # print '**', value
                output.append(value)
    final = '\n'.join(output)
    return final

def setpattern_alloted(text):
    output = []
    for i in range(len(text)):
        if i == 0:
            header = text[i]
            # print 'Header: ', header
        else:
            if text[i] != '':
                data = text[i].split('|')
                sa_order_reference_no = '|'
                transaction_date_time = data[12].split('/')
                transaction_date_time = transaction_date_time[2] + transaction_date_time[1] + transaction_date_time[0]
                if len(transaction_date_time.strip()) < 14:
                    transaction_date_time = transaction_date_time + (
                                '0' * (14 - len(transaction_date_time.strip()))) + '|'
                # print transaction_date_time
                filler = '|'
                sa_code = data[17] + '|'
                if len(transaction_date_time.strip()) < 15:
                    transaction_date_time = transaction_date_time + (
                                ' ' * (15 - len(transaction_date_time.strip()))) + '|'
                # print sa_code
                unitholder_id = data[0].strip() + '|'
                # print unitholder_id
                new_unitholder_reference_no = '|'
                transaction_code = data[21].strip() + '|'
                # print transaction_code
                fund_code = data[2].strip() + '|'
                # print fund_code
                redemption_type = '|'
                amount = '|'
                unit = '|'
                effective_date = '|'
                counter_fund_code = '|'
                payment_type = '|'
                bank_code = '|'
                bank_account = '|'
                cheque_no = '|'
                cheque_date = '|'
                ic_license = data[20].strip() + '|'
                branch_no = data[18].strip() + '|'
                channel = '|'
                force_entry = '|'
                ltf_condition = '|'
                reason_to_sell = '|'
                rmf_capital_gain = '|'
                rmf_capital_amount = '|'
                auto_redeem_fund_code = '|'
                transaction_id = '|'
                status = data[25].strip() + '|'
                amc_order_reference_no = '|'
                allotment_date = '|'
                allotted_nav = '|'
                allotted_amount = data[13].strip() + '|'
                allotted_unit = data[16].strip() + '|'
                fee = '|'
                withholding_tax = '|'
                vat = data[24].strip() + '|'
                brokerage_fee = '|'
                withholding_tax_for = '|'
                amc_pay_date = '|'
                registrar_transaction_flag = '|'
                sell_all_unit_flag = '|'
                settlement_bank_code = '|'
                settlement_bank_account = '|'
                reject_reason = '|'
                chq_branch = '|'
                tax_invoice_no = '|'
                amc_switching_order_reference_no = '|'
                brokerage_fee_vat = '|'
                approval_code = '|'
                nav_date = data[14].strip() + '|'
                credit_card_issuer = '|'
                value = sa_order_reference_no + transaction_date_time + filler + sa_code + unitholder_id + new_unitholder_reference_no \
                        + transaction_code + fund_code + filler + filler + redemption_type + amount + unit + effective_date \
                        + counter_fund_code + filler + payment_type + bank_code + bank_account + cheque_no + cheque_date \
                        + ic_license + branch_no + channel + force_entry + ltf_condition + reason_to_sell + rmf_capital_gain \
                        + rmf_capital_amount + transaction_id + status + amc_order_reference_no + allotment_date + allotted_nav \
                        + allotted_amount + allotted_unit + fee + withholding_tax + vat + brokerage_fee + withholding_tax_for \
                        + amc_pay_date + registrar_transaction_flag + sell_all_unit_flag + settlement_bank_code \
                        + settlement_bank_account + reject_reason + chq_branch + tax_invoice_no + amc_switching_order_reference_no \
                        + filler + brokerage_fee_vat + approval_code + nav_date + filler + credit_card_issuer
                output.append(value)
    final = '\n'.join(output)
    return final

def setpattern_unitholder(data):

    record = []
    keyValue = {}
    arrText = []
    data_output = data.split('\n')
    output_data = {}
    data_value = []
    for idx,data_value in enumerate(data_output) :
        keyValue[idx] = data_value
        keyValue[idx] = keyValue[idx].split(" ")
        # for index,output in enumerate(keyValue[idx]) :
        #     if output != '':
        #         i = 2
    total_unit = ''
    title_name = ''
    pledge_unit = ''
    data_output_unit = ''
    agent_head = ''
    holder_ref_id = ''
    fund_id = ''

    for index,output in enumerate(data_output) :
        if data_output[index][5:10] == ' '*5 :
            agent_head = ''
        else :
            agent_head =  data_output[index][5:10].strip()

        if data_output[index][15:30] == ' '*15 :
            holder_ref_id = ''
        else :
            holder_ref_id =  data_output[index][15:30].strip()

        if data_output[index][0:4] == ' '*5 :
            fund_id = ''
        else :
            fund_id =  data_output[index][0:4].strip()

        if data_output[index][228:246] == ' '*18 :
            total_unit = data_output[index][228:246].strip()
        else :
            total_unit =  data_output[index][228:247].strip()

        if data_output[index][48:66] == ' '*18 :
            title_name = data_output[index][48:66].strip()
        else:
            title_name = data_output[index][48:65].strip()

        if data_output[index][247:263] == ' '*18 :
            onhold_unit = data_output[index][247:263].strip()
        else:
            onhold_unit = data_output[index][247:265].strip()

        if data_output[index][266:282] == ' '*18:
            pledge_unit = data_output[index][266:282].strip()
        else:
            pledge_unit = data_output[index][266:284].strip()

        data_output_unit += agent_head +'||'+ holder_ref_id +'|' + fund_id+'|'+ total_unit + '|' + '|' + title_name + '||' + onhold_unit + '||' + pledge_unit + '|||' + '\n'


    return data_output_unit

@app.route('/api/v1/account', methods=['POST'])
def account():
    try:
        f = request.files['file']
        f.save(secure_filename(f.filename))
        text = readfile(f.filename)
        text = text.splitlines()
        res = setpattern_account(text)
        time_data = datetime.now().strftime("%Y%m%d")
        file_name = "unitholdernew"+time_data+".txt"
        # print file_name
        with open(file_name, 'w') as file:
            file.write(res.encode('utf-8'))
        return json_response({"message": "Convert file success."}, 200)
    except Exception as e:
        return str(e)

@app.route('/api/v1/unitholder', methods=['POST'])
def unitholder_bay_to_fund():
    try:
        filesname = request.files['file']
        filesname.save(secure_filename(filesname.filename))
        text_data = readfile(filesname.filename)
        data_res = setpattern_unitholder(text_data)
        x = datetime.now().strftime("%Y%m%d")
        filename_unit = x+'_AMC_CODE_UNITHOLDERBALANCE.txt'
        with open(filename_unit, 'w') as file_output:
            file_output.write(x+ '|' + 'IAM' + '|' + str(len(text_data.split('\n'))) + '\n')
            file_output.write(data_res)
        return json_response({"message": "Convert file success."}, 200)
    except Exception as e:
        return str(e)

@app.route('/api/v1/approved', methods=['POST'])
def approved_fund_to_bay():

    try:
        f = request.files['file']
        f.save(secure_filename(f.filename))
        today = datetime.now().strftime('%Y%m%d')
        text = readfile(f.filename)
        text = text.splitlines()
        resp = setpattern_approved(text)
        file_name = 'ORDER' + today + '.txt'
        with open(file_name, 'w') as file_output:
            file_output.write(resp.encode('utf-8'))
        return json_response({"message": "Convert file success."}, 200)
    except Exception as e:
        return str(e)

@app.route('/api/v1/alloted', methods=['POST'])
def alloted_bay_to_fund():
    try:
        f = request.files['file']
        f.save(secure_filename(f.filename))
        today = datetime.now().strftime('%Y%m%d')
        text = readfile(f.filename)
        text = text.splitlines()
        resp = setpattern_alloted(text)
        file_name = today + '_AMC_CODE_ALLOTTEDTRANSACTIONS.txt'
        with open(file_name, 'w') as file_output:
            file_output.write(resp.encode('utf-8'))
        return json_response({"message": "Convert file success."}, 200)
    except Exception as e:
        return str(e)

@app.route('/api/v1/nav_bay', methods=['POST'])
def nav_bay():
    try:
        res = setpattern_navbay()
        time_data = datetime.now().strftime("%Y%m%d")
        file_name = "nav_bidoffer"+time_data+".txt"
        with open(file_name, 'w') as file:
                file.write(res.encode('utf-8'))
        return json_response({"message": "Convert file success."}, 200)
    except Exception as e:
        return str(e)



if __name__ == '__main__':
    app.run(host='localhost', port=8080)

# convert_file("20190829_IAM_NEWACCOUNT/20190829_IAM_CUSTOMER.txt")
